<?php

App::uses('Admin.AdminAppController', 'Controller');

class SettingsController extends AdminAppController {

    public $components = array('Session','Paginator', 'Flash');



    public function index() {
        $this->paginate = array('limit' => 10);
        $settings = $this->paginate('Setting');
        
        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Setting']['keyword'];
            $settings = $this->Setting->find('all', array(
                'fields' => array('id', 'name', 'value', 'updated'),
                'conditions' => array(
                    'OR' => array('name like' => '%' . $keyword . '%',
                        'value like' => '%' . $keyword . '%'))
            ));
        }
         $this->set('settings', $settings);
    }


    public function add() {
        if ($this->request->is('post')) {
            // $this->request->data['Catagory']['slug'] = $this->Tool->slug($this->request->data['Catagory']['name']);
            $this->Setting->create();
            if ($this->Setting->save($this->request->data)) {
                $this->Session->setFlash(__('The Setting has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The Setting could not be saved. Please, try again.'));
            }
        }
    }

    public function delete($id = null) {
        $this->Setting->id = $id;
        if (!$this->Setting->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Setting->delete()) {
            $this->Flash->success(__('The Setting has been deleted.'));
        } else {
            $this->Flash->error(__('The Setting could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid Setting'));
        }

        $settings = $this->Setting->findById($id);
        if (!$settings) {
            throw new NotFoundException(__('Invalid Setting'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Setting->id = $id;
            if ($this->Setting->save($this->request->data)) {
                $this->Flash->success(__('The setting has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update The setting.'));
        }

        if (!$this->request->data) {
            $this->request->data = $settings;
        }
        $this->set('settings', $settings);
    }

}
