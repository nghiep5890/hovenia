<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

//App::uses('Controller', 'AppController');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AdminAppController extends Controller {
	public $components = array(
			'DebugKit.Toolbar',
			'Paginator',
			'Flash',
			'Session',
			'Auth' => array(
				'authenticate' => array(
					'Form' => array(
						'passwordHasher' => 'Blowfish'
					)
				),
                            'loginAction' => array('plugin'=>'admin', 'controller'=>'users', 'action'=>'login'),
			    'logoutRedirect' => '/',
			    'loginRedirect' => array(
                                'plugin' =>'admin',
                                'controller'=> 'pages',
                                'action'=>'index'),
//                            'authorize' => array('Controller')
//				'authorize' => array(
//					'Actions' => array('actionPath' => 'controllers')
//				)
			)
		);

	public function beforeFilter() {
            $group_id = $this->Auth->user()['group_id'];
            	// die($group_id);
                if($group_id != 2){
                    return $this->redirect('/');
                } 
			$this->Auth->allow();

                $this->loadModel('Setting');
                $settings = $this->Setting->find("all");
                $this->set('settings', $settings);
                $site_logo = $this->Setting->find("first", array('conditions' => array('name' => 'site_logo')));
			$this->set('site_logo', $site_logo);
                $site_name = $this->Setting->find("first", array('conditions' => array('name' => 'site_name')));
			$this->set('site_name', $site_name);
	}
}


