<?php

App::uses('Admin.AdminAppController', 'Controller');

class FaqsController extends AdminAppController {

    public $components = array('Paginator', 'Flash');

    public function index() {
        $this->paginate = array('limit' => 10);
        $faqs = $this->paginate('Faq');
        // Search
        if ($this->request->is('post')) {
            $keyword = $this->request->data['Faq']['keyword'];
            $faqs = $this->Faq->find('all', array(
                'fields' => array('id','question','answer'),
                'conditions' => array(
                    'OR' => array('question like' => '%' . $keyword . '%',
                        'answer like' => '%' . $keyword . '%'))
                ));
        }
        $this->set('faqs', $faqs);
    }

    public function view($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid faq'));
        }

        $faqs = $this->Faq->findById($id);
        if (!$faqs) {
            throw new NotFoundException(__('Invalid faq'));
        }
        $this->set('faqs', $faqs);
    }

    public function add() {
        if ($this->request->is('post')) {
            $this->Faq->create();
            if ($this->Faq->save($this->request->data)) {
                $this->Session->setFlash(__('The Faq has been saved.'));
                return $this->redirect(array('action' => 'index '));
            } else {
                $this->Flash->error(__('The Faq could not be saved. Please, try again.'));
            }
        }
    }

    public function delete($id = null) {
        $this->Faq->id = $id;
        if (!$this->Faq->exists()) {
            throw new NotFoundException(__('Invalid Faq'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Faq->delete()) {
            $this->Flash->success(__('The Faq has been deleted.'));
        } else {
            $this->Flash->error(__('The Faq could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid faqs'));
        }

        $faqs = $this->Faq->findById($id);
        if (!$faqs) {
            throw new NotFoundException(__('Invalid faqs'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $this->Faq->id = $id;
            if ($this->Faq->save($this->request->data)) {
                $this->Flash->success(__('The Faq has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Flash->error(__('Unable to update The Faq.'));
        }

        if (!$this->request->data) {
            $this->request->data = $faqs;
        }
    }

}
