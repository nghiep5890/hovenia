<div id="controls">
    <div class="add"  style="float: right">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'categories',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>
    <div class="col-md-4">
        <?=  $this->Form->create('Category', array('type' => 'post')); ?>
        <?=  $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?=$this->Form->end(); ?>
    </div>
    
</div>
<table class="table">
<thead>
    <tr>   
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('image'); ?></th>
        <th><?php echo $this->Paginator->sort('info'); ?></th>
        <th>Actions</th>
    </tr>
</thead>
<tbody>
    <?=  $this->Form->create('Category', array('escape' => false)); ?>
    <?php if(empty($categories)){?>
        <h4 style="color : red">No found categories</h4>;
    <?php      } else{ 
                foreach ($categories as $categories){ ?>
    <tr>
        <td><?= $categories['Category']['id']; ?></td>
        <td><?= $categories['Category']['name']; ?></td>
        <td><?php echo $this->Html->image("../images/".$categories['Category']['image'],array('height'=>'50px')); ?></td>
        <td><?= $categories['Category']['info']; ?></td>
        <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $categories['Category']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $categories['Category']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $categories['Category']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $categories['Category']['id']))); ?>
        </td>
    </tr>    
    <?php }} ?>
    <?= $this->Form->end(); ?>
</tbody>
</table>
<?php
    echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled'));
    echo " | ".$this->Paginator->numbers()." | "; 
    echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled'));
    echo "&nbsp;&nbsp;&nbsp;";
    echo " Page ".$this->Paginator->counter();
?>