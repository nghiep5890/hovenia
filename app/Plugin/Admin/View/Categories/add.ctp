<div class="productadd">
	<?php echo $this->Form->create('Category',array('type'=>'file'));  ?>
		<fieldset>
			<legend><?php echo __('Add New Category'); ?></legend>
			<dl>
				<dt>Name:</dt><dd><?= $this->Form->input('name',array('label' =>false,'required' => 'required')); ?></dd>
				<dt>Info:</dt><dd><?= $this->Form->input('info',array('label' => false,'type' => 'textarea','class'=>'ckeditor')); ?></dd>
				<dt>Image:</dt><dd><?= $this->Form->input('image',array('label' => false,'type'=>'file')); ?></dd>
				<dt>Published(0 : not display; 1 : display;):</dt><dd><?= $this->Form->input('published',array('label' => false,'options' => array('0','1'),'selected' => '1')); ?></dd>
			</dl>

		</fieldset>
	<?php  echo $this->Form->button('Reset', array('type' => 'reset'));
                echo $this->Form->end(__('Save')); ?>
</div>