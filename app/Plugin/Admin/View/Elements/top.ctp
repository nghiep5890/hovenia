<div id="cover">
    <div class="col-md-10">
        <h2><?php echo __('Admin Panel'); ?></h2>
    </div>
    <div class="top-right col-md-2">
        <div class="btn-admin-ctrl">
            <div class="admin-ctrl">
                   <?php 
//                   pr(AuthComponent::user());
                   if (!empty(AuthComponent::user()['avatar'])) { ?>

                <p><?php
                            $avatar = AuthComponent::user()['avatar'];
                               echo $this->Html->image("../images/avatar/".$avatar, array('style' => ('height:35px;'))); ?></p>
                   <?php }else{ ?>
                    <p><?php  echo $this->Html->image("../images/avatar/avatar-user.png", array('style' => ('height:35px;'))); ?></p>
                <?php } ?>
                <h3>
                    <?php
                        $fullname = AuthComponent::user()['last_name'].' '.
                                    AuthComponent::user()['first_name'];
                        $id = AuthComponent::user()['id'];?>
                        <div class="dropdown">
                            <?php  echo $this->Html->link($fullname,array(
                                                    'controller' => 'users',
                                                    'action' => 'view',
                                                    $id => $id),
                                                    array('style' => 'text-transform:capitalize')
                                                    );
                                    if (AuthComponent::user()) { ?>
			    		<div class="acc-down">
                                            <p><a href="/admin/users/edit/<?php echo $id;?>">Edit</a></p>
                                            <p><?php echo $this->Html->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></p>
		    			</div>
                                    <?php } ?>
	    		</div>
                </h3>
            </div>
        </div>
    </div>
</div>
