<div id="left">
    <div id="logo">
        <a class="logo" href="/">
            <p><?php echo isset($site_logo['Setting']['value']) ? $site_logo['Setting']['value'] : ''; ?></p>
            <span></span>
        </a>
    </div>
    <div class="menu-admin">
        <ul>
            <li></i><?php echo $this->Html->link('<i class="fa fa-home" aria-hidden="true"></i> Dashboard', array('plugin'=>'admin','controller' => 'pages', 'action' => 'index'),array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-list-ol" aria-hidden="true"></i> Categories', array('plugin'=>'admin','controller' => 'categories', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-cutlery" aria-hidden="true"></i> Products', array('plugin'=>'admin','controller' => 'products', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-commenting-o" aria-hidden="true"></i> Reviews', array('plugin'=>'admin','controller' => 'reviews', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-gift" aria-hidden="true"></i> Coupons', array('plugin'=>'admin','controller' => 'coupons', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-comments" aria-hidden="true"></i> FAQs', array('plugin'=>'admin','controller' => 'faqs', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-shopping-basket" aria-hidden="true"></i> Orders', array('plugin'=>'admin','controller' => 'orders', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-group" aria-hidden="true"></i> Groups', array('plugin'=>'admin','controller' => 'groups', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i> Users', array('plugin'=>'admin','controller' => 'users', 'action' => 'index'), array('escape' => false)); ?></li>
            <li><?php echo $this->Html->link('<i class="fa fa-gears" aria-hidden="true"></i> Page Setting', array('plugin'=>'admin','controller' => 'settings', 'action' => 'index'), array('escape' => false)); ?></li>    
        </ul>
    </div>
</div>
