<div id="controls">
    <div class="col-md-4">
        <?=  $this->Form->create('Order', array('type' => 'post')); ?>
        <?=  $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?=$this->Form->end(); ?>
    </div>
    
</div>

<table class="table">
<thead>
    <tr>
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th>Customer</th>
        <th>Quantity</th>
        <th><?php echo $this->Paginator->sort('total');?></th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    <?php if(empty($orders)){?>
        <h4 style="color : red">No found Order</h4>;
    <?php   } else{ 
                foreach ($orders as $orders){ 
                $data = array(
                                'customer_info' => json_decode($orders['Order']['customer_info']),
                                'order_info' => json_decode($orders['Order']['order_info']),
                                'payment_info' => json_decode($orders['Order']['payment_info'])
                              );
                ?>
    <tr>
        <td><?= $orders['Order']['id']; ?></td>
        <td><?php if($orders['Order']['user_id']) {
                        echo $orders['User']['username'];
                    } else {
                        echo $data['customer_info']->name;
                    }?></td>
        <td><?php echo $data['payment_info']->quantity_; ?></td>
        <td><?php echo $data['payment_info']->total."$"; ?></td>
        <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $orders['Order']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $orders['Order']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $orders['Order']['id']))); ?>
        </td>
    </tr>   
    <?php }} ?>
</tbody>
</table>
<div class="paging">
<?php
    echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); 
    echo " | ".$this->Paginator->numbers()." | "; 
    echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled')); 
    echo "&nbsp;&nbsp;&nbsp;";
    echo " Page ".$this->Paginator->counter(); 
?>
</div>
