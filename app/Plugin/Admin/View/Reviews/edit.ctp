<legend><?php echo __('Edit Review'); ?></legend>
<div class="edit">
    <?php
        echo $this->Form->create('Review');
        echo '<b>Username :   </b>'.$reviews['User']['username'].'<br><br/>';
        echo '<b>Product :    </b>'.$reviews['Product']['name'].'<br><br/>';
        echo $this->Form->input('review');
        echo $this->Form->input('approve');
        echo $this->Form->input('id', array('type' => 'hidden'));
        echo $this->Form->end('Save');
    ?>
</div>
