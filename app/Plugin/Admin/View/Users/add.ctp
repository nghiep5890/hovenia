<div class="productadd">
    <?php echo $this->Form->create('Users',array('type'=>'file')); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <dl>
            <dt>Username :</dt><dd><?= $this->Form->input('username',array('label'=>false,'id' => 'add-user','class'=> 'add-user','placeholder' => 'Username must be between 5 and 32 characters', 'pattern' => "^[a-z0-9]{5,32}$", 'required' => 'required', 'autofocus')); ?></dd>
            <dt>Password :</dt><dd><?= $this->Form->input('password',array('label'=>false,'id' => 'add-user','class'=> 'add-user','placeholder' => '8-32 characters, at least one number and one uppercase letter','pattern' => "(?=.*\d)(?=.*[A-Z]).{8,32}", 'required' => 'required')); ?></dd>
            <dt>First Name :</dt><dd><?= $this->Form->input('first_name',array('label'=>false)); ?></dd>
            <dt>Last Name :</dt><dd><?= $this->Form->input('last_name',array('label'=>false)); ?></dd>
            <dt>Avatar :</dt><dd><?= $this->Form->input('avatar',array('type'=>'file','label'=>false)); ?></dd>
            <dt>Email :</dt><dd><?= $this->Form->input('email',array('label'=>false,'type'=>'email','required' => 'required')); ?></dd>
            <dt>Date of birth :</dt><dd><?= $this->Form->input('birth', array(
                        'label' => false,
                        'dateFormat' => 'DMY',
                        'minYear' => date('Y') - 80,
                        'maxYear' => date('Y') - 16,
                )); ?></dd>
            <dt>Homephone :</dt><dd><?= $this->Form->input('homephone', array('label'=>false)); ?></dd>
            <dt>Mobilephone :</dt><dd><?= $this->Form->input('mobilephone', array('label'=>false)); ?></dd>
            <dt>Address :</dt><dd><?= $this->Form->input('address', array('label'=>false)); ?></dd>
        </dl>
    </fieldset>
    <?php  echo $this->Form->button('Reset', array('type' => 'reset'));
            echo $this->Form->end(__('Add User')); ?>
</div>