<legend><?php echo __('Edit User'); ?></legend>
<div class="container">
      <aside class="col-sm-3 sidebar-seting">
  
      </aside>
      <article class="col-sm-9">
          <div class="Users form">
          <?php echo $this->Form->create('Users',array('type'=>'file')); ?>
              <fieldset>
             <?php
                  echo $this->Form->input('first_name');
                  echo $this->Form->input('last_name');
                  echo $this->Form->input('avatar');
                if(!empty($users['Users']['avatar'])){
                  echo $this->Html->image("../images/avatar/".$users['Users']['avatar'],array('height'=>'50px'));
                } else{
                  echo $this->Html->image("../images/avatar/avatar-user.png",array('height'=>'50px'));

                }
                  echo $this->Form->input('avatar_new',array('type'=>'file'));
                  echo $this->Form->input('username');
                  echo $this->Form->input('password');
                  echo $this->Form->input('email');
                  echo $this->Form->input('birth', array(
                      'label' => 'Date of birth',
                      'dateFormat' => 'DMY',
                      'minYear' => date('Y') - 80,
                      'maxYear' => date('Y') - 16,
                  ));
                  echo $this->Form->input('homephone');
                  echo $this->Form->input('mobilephone');
                  echo $this->Form->input('address');
              ?>
              </fieldset>
          <?php echo $this->Form->end(__('Submit')); ?>
          </div>
      </article>
 </div>
