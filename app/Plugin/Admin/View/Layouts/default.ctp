<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $this->fetch('title'); ?></title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->css(array('../admin/css/bootstrap.min', '../admin/css/style','../css/font-awesome.min'));
        ?>

        <script type="text/javascript">
            var WEB_ROOT = "<?php echo $this->App->webroot('/'); ?>";
            var urlhome = "<?php echo Router::url('/admin', true); ?>";
            var access_token = "<?php echo $access_token; ?>";
            var unbind = 0;
            history.pushState(null, null, document.title);
            window.addEventListener('popstate', function () {
                history.pushState(null, null, document.title);
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                <?php echo $this->element('top'); ?>
            </div>

            <div id="flashMessage">
                <?php echo $this->Session->flash(); ?>
            </div>

            <div id="content">
                <div class="col-md-2">
                    <?php echo $this->element('left'); ?>
                </div>
                <div class="col-md-10">
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>

            <div id="footer">
                
                    <div id="coppyright">
                        <span>coppyright &copy; 2016 <?php echo ($site_name['Setting']['value']); ?> - all rights reserved</span>
                    </div>
                
            </div>
        </div>
    </body>
</html>
