    <div id="controls">

    <?=  $this->Form->create('Control', array('type' => 'post')); ?>
    <div class="col-md-4">
    <?=  $this->Form->input('search',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
    </div>
    <div class="view col-md-4">
    <?=  $this->Form->input('Show:',array('class'=>'control','options' => array('10','15','20','30','50'),'after'=>' /page')); ?>
    <?=  $this->Form->input('Sort by:',array('class'=>'control','options' => array('id','question'))); ?>
    </div>
    <div class="add col-md-4">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'faqs',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>

    <?=$this->Form->end; ?>
</div>
<table class="table">
<thead>
    <tr>   
        <th>Id</th>
        <th>Question</th>
        <th>Answer</th>
    </tr>
</thead>
<tbody>
    <?php foreach ($faqs as $faqs) : ?>
    <tr>
        <td><?= $faqs['Faq']['id']; ?></td>
        <td><?= $faqs['Faq']['question']; ?></td>
        <td><?= $faqs['Faq']['answer']; ?></td>
        <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $faqs['Faq']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $faqs['Faq']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $faqs['Faq']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $faqs['Faq']['id']))); ?>
        </td>
        <td><?= $this->Form->input('role', array('options' => array('0','1'))); ?></td>
    </tr>    
    <?php endforeach; ?>
</tbody>
</table>