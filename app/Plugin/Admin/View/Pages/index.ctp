<div class="admin-home">
	<div class="">
		<div class="stats-today">
			<div class="stats- stats-today-visitors">
				<h3>visitors</h3>
				<div class="stats--">
					<h4>Today: 128</h4>
					<h4>This Month: 11231</h4>
				</div>
			</div>
			<div class="stats- stats-today-users">
				<h3>new user</h3>
				<div class="stats--">
					<h4>Today: 006</h4>
					<h4>This Month: 100</h4>
				</div>
			</div>
			<div class="stats- stats-today-oders">
				<h3>new order</h3>
				<div class="stats--">
					<h4>Today: 005</h4>
					<h4>This Month: 213</h4>
				</div>
			</div>
			<div class="stats- stats-today-sales">
				<h3>sales</h3>
				<div class="stats--">
					<h4>Today: 200 &#36;</h4>
					<h4>This Month: 15000 &#36;</h4>
				</div>
			</div>
		</div>
	</div>
	<div class="row monthly-stats">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Visitors Stats</div>
				<div class="panel-body">
					<div class="chart">
						<div id="chartVisitors" style="height: 300px; width: 100%;"></div>
					</div>
					<div id="legendDiv"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row monthly-stats">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Orders Stats</div>
				<div class="panel-body">
					<div class="chart">
						<div id="chartOrders" style="height: 300px; width: 100%;"></div>
					</div>
					<div id="legendDiv"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row monthly-stats">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Sales Stats</div>
				<div class="panel-body">
					<div class="chart">
						<div id="chartSales" style="height: 300px; width: 100%;"></div>
					</div>
					<div id="legendDiv"></div>
				</div>
			</div>
		</div>
	</div>
</div>

	<!-- Loading Scripts -->
	<script src="/admin/js/Chart.min.js"></script>
	<script src="/admin/js/chartData.js"></script>
	<script src="/admin/js/canvasjs.min.js"></script>
	
	<script>
	
		window.onload = function(){

			// Line chart from Data (chartData.js)
			var chart = new CanvasJS.Chart("chartVisitors", (visitorsData));
			    chart.render();

			var chart = new CanvasJS.Chart("chartOrders", (ordersData));
			    chart.render();
			
			var chart = new CanvasJS.Chart("chartSales", (salesData));
			    chart.render();
		}
	</script>	