<div id="controls">
    <div class="add"  style="float: right">
        <div class="addButton">
            <?= $this->Html->link(
                    $this->Html->tag('i', '', array('class' => 'fa fa-plus fa-fw')).'Add',
                    array(
                        'controller' => 'products',
                        'action' => 'add'),
                    array('escape' => false)
            ); ?>
        </div>
    </div>
    <div class="col-md-4">
        <?=  $this->Form->create('Product', array('type' => 'post')); ?>
        <?=  $this->Form->input('keyword',array('class'=>'span3','label'=>false, 'placeholder'=>'Search...')); ?>
        <?=$this->Form->end(); ?>
    </div>
    
</div>

<table class="table">
<thead>
    <tr>   
        <th><?php echo $this->Paginator->sort('id');?></th>
        <th><?php echo $this->Paginator->sort('name'); ?></th>
        <th><?php echo $this->Paginator->sort('path_img1','Image'); ?></th>
        <th style="width : 40%;"><?php echo $this->Paginator->sort('description'); ?></th>
        <th><?php echo $this->Paginator->sort('created'); ?></th>
        <th>Action</th>
        <th><?php echo $this->Paginator->sort('published'); ?></th>
    </tr>
</thead>
<tbody>
    <?php if(empty($product)){?>
        <h4 style="color : red">No found products</h4>;
    <?php   } else{ 
                foreach ($product as $product){ ?>
    <tr>
        <td><?= $product['Product']['id']; ?></td>
        <td><?= $product['Product']['name']; ?></td>
        <td><?= $this->Html->image("../images/products/".$product['Product']['path_img1'],array('height'=>'50px')); ?></td>
        <td><?= $product['Product']['description']; ?></td>
        <td><?= $product['Product']['created']; ?></td>
        <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $product['Product']['id'])); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id'])); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $product['Product']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $product['Product']['id']))); ?>
        </td>
        <td><?php if($product['Product']['published']==1){ ?>
                    <i class="fa fa-check fa-fw" style="color : green"></i>
            <?php  ;    } else {?>
                    <i class="fa fa-close fa-fw" style="color : red"></i>
            <?php  ; }?>
        </td>
    </tr>    
    <?php }} ?>
</tbody>
</table>
<div class="paging">
<?php
    echo $this->Paginator->prev('« Previous ', null, null, array('class' => 'disabled')); 
    echo " | ".$this->Paginator->numbers()." | "; 
    echo $this->Paginator->next(' Next »', null, null, array('class' => 'disabled')); 
    echo "&nbsp;&nbsp;&nbsp;";
    echo " Page ".$this->Paginator->counter(); 
?>
</div>