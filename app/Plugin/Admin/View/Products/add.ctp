<div class="productadd">
	<?php echo $this->Form->create('Product',array('type'=>'file'));  ?>
		<fieldset>
			<legend><?php echo __('Add New Product'); ?></legend>
			<dl>
				<dt>Category:</dt><dd><?= $this->Form->input('category_id',array('label' => false,'options'=>$category)); ?></dd>
				<dt>Name:</dt><dd><?= $this->Form->input('name',array('label' =>false,'required' => 'required')); ?></dd>
				<dt>Decription:</dt><dd><?= $this->Form->input('description',array('label' => false,'type' => 'textarea','class'=>'ckeditor')); ?></dd>
				<dt>Image1:<br>(Image represent product)</dt><dd><?= $this->Form->input('path_img1',array('label' => false,'type'=>'file')); ?></dd>
				<dt>Image2:</dt><dd><?= $this->Form->input('path_img2',array('label' => false,'type'=>'file')); ?></dd>
				<dt>Image3:</dt><dd><?= $this->Form->input('path_img3',array('label' => false,'type'=>'file')); ?></dd>
				<dt>Image4:</dt><dd><?= $this->Form->input('path_img4',array('label' => false,'type'=>'file')); ?></dd>
				<dt>Price:</dt><dd><?= $this->Form->input('price',array('label' => false,'type' => 'number','min'=>0,'required' => 'required')); ?></dd>
				<dt>Expiry:</dt><dd><?= $this->Form->input('expiry',array('label' => false,'type' => 'number','min'=>0,'required' => 'required')); ?></dd>
				<dt>Sale Off:</dt><dd><?= $this->Form->input('sale_off',array('label' => false,'type' => 'money')); ?></dd>
				<dt>Sale Start:</dt><dd><?= $this->Form->input('sale_start',array('label' => false)); ?></dd>
				<dt>Sale End:</dt><dd><?= $this->Form->input('sale_end',array('label' => false)); ?></dd>
				<dt>Rate:</dt><dd><?= $this->Form->input('rating',array('label' => false,'options' => array('0','1','2','3','4','5'),'selected' => '3','required' => 'required')); ?></dd>
                                <dt>New:</dt><dd><?= $this->Form->input('new',array('label' => false,'options' => array('0','1'),'selected' => '1')); ?></dd>
                                <dt>Recommended:</dt><dd><?= $this->Form->input('recommended',array('label' => false,'options' => array('0','1'),'selected' => '1')); ?></dd>
                                <dt>Cold-storage:</dt><dd><?= $this->Form->input('cold-storage',array('label' => false,'options' => array('0','1'),'selected' => '1')); ?></dd>
				<dt>Available:</dt><dd><?= $this->Form->input('available',array('label' => false,'type' => 'text')); ?></dd>
				<dt>Referenses:</dt><dd><?= $this->Form->input('referenses',array('label' => false,'type' => 'link')); ?></dd>
				<dt>Published:<br>(0 : not display; 1 : display;)</dt><dd><?= $this->Form->input('published',array('label' => false,'options' => array('0','1'),'selected' => '1')); ?></dd>
			</dl>

		</fieldset>
	<?php  echo $this->Form->button('Reset', array('type' => 'reset'));
                echo $this->Form->end(__('Save')); ?>
</div>