
<div class="container content">
        <!--        ///////////////         MenuLeft         //////////////       -->
        <?= $this->element('aside'); ?>
        <!--        ///////////////         ARTICLE         //////////////       -->
    <article>
		<div class="users index">
		    <!-- <h2><?php echo __('Users'); ?></h2> -->
		    <table cellpadding="" cellspacing="0">
			<thead>
			<tr>
					<th>Id</th>
					<th>Avatar</th>
					<th>Full Name</th>
		            <th>Birthday</th>
		            <th>Email</th>
		            <th>MobilePhone</th>
		            <th>Address</th>
					<th class="actions">Actions</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td><?php echo ($users['User']['id']); ?></td>
				<td><?php if(!empty($users['User']['avatar'])){
                  echo $this->Html->image("../images/avatar/".$users['User']['avatar'],array('height'=>'50px'));
                } else{
                  echo $this->Html->image("../images/avatar/avatar-user.png",array('height'=>'50px'));
                } ?></td>
				<td><?php echo ($users['User']['first_name']." ".$users['User']['last_name']); ?></td>
		        <td><?php echo ($users['User']['birth']); ?></td>
		        <td><?php echo ($users['User']['email']); ?></td>
		        <td><?php echo ($users['User']['mobilephone']); ?></td>
		        <td><?php echo ($users['User']['address']); ?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $users['User']['id'])); ?>
				</td>
			</tr>
			</tbody>
		    </table>
		</div>
	</article>
</div>