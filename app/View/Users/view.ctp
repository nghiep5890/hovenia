	
<div class="container content">
    
		<div class="users-view">
			<dl>
		<dt><?php echo __('Full Name'); ?></dt>
		<dd>
			<?php echo ($users['User']['first_name']." ".$users['User']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Avatar'); ?></dt>
		<dd>
			<?php if(!empty($users)){
                  echo $this->Html->image("../images/avatar/".$users['User']['avatar']); 
                } else{
                  echo $this->Html->image("../images/avatar/avatar-user.png",array('height'=>'20px'));
                }?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo ($users['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Homephone'); ?></dt>
		<dd>
			<?php echo ($users['User']['homephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobilephone'); ?></dt>
		<dd>
			<?php echo ($users['User']['mobilephone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adddress'); ?></dt>
		<dd>
			<?php echo ($users['User']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo ($users['User']['created']); ?>
			&nbsp;
		</dd>
</dl>
		</div>
</div>