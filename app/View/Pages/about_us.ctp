<!-- ///////////////////////////         CONTENT         //////////////////////////      -->
<section id="content">
    <div class="top-banner"><!-- ///////////////////////////         BANNER         //////////////////////////      -->
        <h2>about us</h2>
        <div class="page-link">
            <h3>Home
                <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>	
                <span style="color:#fff;">About us</span></h3>
        </div>
    </div>
    <div class="container content">
        <!--        ///////////////         CATEGORY         //////////////       -->
        <?= $this->element('aside'); ?>
        <!--        ///////////////         ARTICLE         //////////////       -->
        <article>
            <div class="main-aboutus">
                <div class="about-story">
                    <div class="about-story-img">
                        <img src="images/aboutus_chef313x488.jpg" alt="">
                    </div>
                    <div class="about-story-text">
                        <h1 class="contenth1">Let us tell you</h1>
                        <h2 class="contenth2">story About us</h2>
                        <hr>
                        <p class="p1">l</p>
                        <p class="p2">ook at the sunset, life is amazing, life is beautiful, life is what you make it. In life there will be road blocks but we will over c is a major key to success. Don’t ever play yourself. They will try to close the door on you, just open it. Surround yourself with angels, positive energy. They will try to close the door on you, just open it. Major key, don’t fall for the trap, stay focused. It’s the ones closest to you that want to see you fail.</p>
                    </div>
                    <div class="statisctic">
                        <div class="sta-gr">
                            <div class="sta-count">
                                <span>229</span>
                                <p>happy clients</p>
                            </div>
                            <div class="sta-count">
                                <span>109</span>
                                <p>products in store</p>
                            </div>
                            <div class="sta-count" style="border-right:none;">
                                <span>22</span>
                                <p>awesome staffs</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-gallery">
                    <h1 class="contenth1">Discover now</h1>
                    <h2 class="contenth2">our image gallery</h2>
                    <hr>
                    <div class="img-top">
                        <div class="img-top-l">
                            <a href=""><img style="margin-bottom:26px;" src="images/aboutus_img1.jpg" alt=""></a>
                            <a href=""><img src="images/aboutus_img2.jpg"></a>
                        </div>
                        <div class="img-top-r">
                            <a href=""><img src="images/aboutus_img3.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="img-bot">
                        <div class="img-bot-l">
                            <a href=""><img src="images/aboutus_img4.jpg" alt=""></a>
                        </div>
                        <div class="img-bot-r">
                            <i class="fa fa-long-arrow-right fa-fw"></i>
                            <?php echo $this->Html->link('discover our products', array('controller' => 'products', 'action' => 'index')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>