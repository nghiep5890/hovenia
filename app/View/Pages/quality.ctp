<!-- ///////////////////////////         CONTENT         //////////////////////////      -->
<section id="content">
    <div class="top-banner"><!-- ///////////////////////////         BANNER         //////////////////////////      -->
        <h2>quality</h2>
        <div class="page-link">
            <h3>Home
                <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>	
                <span style="color:#fff;">quality</span></h3>
        </div>
    </div>
    <div class="container content">
        <!--        ///////////////         CATEGORY         //////////////       -->
        <?= $this->element('aside'); ?>        <!--        ///////////////         ARTICLE         //////////////       -->
        <article>
            <div class="quality quality-1">
                <img src="images/quality_img1.jpg">
                <div class="quality-des">
                    <h3>production process</h3>
                    <p>The key to success is to keep your head above the water, never give up. The first of the month is coming, we have to get money, we have no choice. It cost money to eat and they don’t want you to eat. Life is what you make it, so let’s make it. We don’t see them, we will never see them.</p>
                </div>
            </div>
            <hr class="hr-qua">
            <div class="quality quality-2">
                <img src="images/quality_img2.jpg">
                <div class="quality-des">
                    <h3>kind of material</h3>
                    <p>The key to success is to keep your head above the water, never give up. The first of the month is coming, we have to get money, we have no choice. It cost money to eat and they don’t want you to eat. Life is what you make it, so let’s make it. We don’t see them, we will never see them.</p>
                </div>
            </div>
            <hr class="hr-qua">
            <div class="quality quality-3">
                <img src="images/quality_img3.jpg">
                <div class="quality-des">
                    <h3>hygiene and food safety</h3>
                    <p>The key to success is to keep your head above the water, never give up. The first of the month is coming, we have to get money, we have no choice. It cost money to eat and they don’t want you to eat. Life is what you make it, so let’s make it. We don’t see them, we will never see them.</p>
                </div>
            </div>
            <hr class="hr-qua">
            <div class="quality quality-4">
                <img src="images/quality_img4.jpg">
                <div class="quality-des">
                    <h3>production technology</h3>
                    <p>The key to success is to keep your head above the water, never give up. The first of the month is coming, we have to get money, we have no choice. It cost money to eat and they don’t want you to eat. Life is what you make it, so let’s make it. We don’t see them, we will never see them.</p>
                </div>
            </div>
        </article>
    </div>