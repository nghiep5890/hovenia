<?php
$cakeDescription = 'Bakery';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('jquery-ui.css'); ?>
    <?= $this->Html->script('jquery-2.2.4.min.js') ?>
    <?= $this->Html->script('responsiveslides.min.js') ?>
    <?= $this->Html->script('jquery-ui.js') ?>
    <?= $this->Html->script('wowslider.js') ?>
    <?= $this->Html->script('script.js') ?>
    <?= $this->Html->script('script.js') ?>
    <?= $this->Html->script('jquery.flexslider.js') ?>
    
</head>
<body>
    
    <div id="#wrapper">
	    <?= $this->element('login') ?>
        <?= $this->element('header') ?>
        <?= $this->element('nav') ?>
            <?= $this->fetch('content') ?>
        <?= $this->element('newsletter') ?>
        <?= $this->element('footer') ?>
    </div>
</body>
</html>