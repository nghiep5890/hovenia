<!-- ///////////////////////////         CONTENT         //////////////////////////      -->
<section id="content">
    <div class="top-banner"><!-- ///////////////////////////         BANNER         //////////////////////////      -->
        <h2>Product</h2>
        <div class="page-link">
            <h3>Home
                <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>
                Category
                <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>
                <span style="color:#fff;"><?php echo isset($category['Category']['name']) ? $category['Category']['name'] : ''; ?></span></h3>
        </div>
    </div>
    <div class="container content">
        <!-- ///////////////////////////         MENU LEFT         //////////////////////////      -->
        <?= $this->element('aside'); ?>
        <!-- ///////////////////////////         ARTICLE         //////////////////////////      -->
        <article>
            <div class="products-group">
                <?php                    
                    $i = 1; $a = 1;
                    if($products){          
                    foreach ($products as $key => $product_se){ ?>
                    <?php if(($key+1)%3) { ?>
                    <div class="prod-itemsp item1 product_view_<?php echo $i; ?>">
                    <?php }else{ ?>
                    <div class="prod-itemsp item2 product_view_<?php echo $i; ?>">
                    <?php } ?>
                        <div class="prod-item">
                                        <?php $id = $product_se['Product']['id']; ?>
                                        <div class="col-md-5 prod-info">
                                        <div class="prod-img">
                                            <?php echo $this->Html->image('../images/products/'.$product_se['Product']['path_img1']); ?>
                                            <div class="ashow">
                                                <a style="cursor:pointer;" class="show" id-product = "<?php echo $product_se['Product']['id']; ?>" product-view = "detail_product_in_list_<?php echo $a; ?>" onclick="quickviewP(this);">quick view</a>
                                            </div>
                                            <?php if($product_se['Product']['new']==1){?>
                                            <div class="new">
                                            <?php echo $this->Html->image('../images/prod_new.jpg'); ?>
                                            </div>
                                             <?php } ?>
                                            <?php if($product_se['Product']['recommended']==1){?>
                                            <div class="star">
                                            <?php echo $this->Html->image('../images/prod-star.png');?>
                                            </div>
                                            <?php } ?>
                                            <?php if (($product_se['Product']['sale_off'] > 0) && ((date('Y-m-d', strtotime($product_se['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d', strtotime($product_se['Product']['sale_end'])) > date('Y-m-d')))) {?>
                                            <div class="saleoff">
                                                <span style="color: white;font-family: Montserrat-Regular;font-size: 12px;"><?php echo '-'.$product_se['Product']['sale_off'].'%'; ?></span>
                                            </div>
                                             <?php } ?>
                                        </div>
                                        <h4><?php echo $product_se['Product']['name']; ?></h4>
                                <?php echo $this->Html->image('../images/rate'.$product_se['Product']['rating'].'.png'); ?>
                                <p><?php echo $this->Text->truncate(($product_se['Product']['description']),80); ?></p>
                                <h2>&#36;
                                    <?php
                                    if (($product_se['Product']['sale_off'] > 0) && ((date('Y-m-d', strtotime($product_se['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d', strtotime($product_se['Product']['sale_end'])) > date('Y-m-d')))) {
                                        echo $product_se['Product']['price'] * (100 - $product_se['Product']['sale_off']) / 100;
                                    } else {
                                        echo $product_se['Product']['price'];
                                    }
                                    ?>
                                    <span>&#36;<?php echo $product_se['Product']['price']; ?></span>
                                </h2>
                                <div class="btn-order">
                                    <form accept-charset="utf-8" method="post" id="ProductIndexForm" action="/products/add_to_cart/<?php echo $product_se['Product']['id'];?>">
                                        <input type="text" name="quantity" value="1" style="display:inline" />
                                        <input type="submit" value="Order" class="btn btn-add-cart"/>
                                    </form>
                                </div>
                                </div>
                        </div>
                    </div>
                    <?php if($i%3 == 0): ?>
                        <div class="detail_product_in_list_<?php echo $a; ?>"></div>
                    <?php $a++; endif; ?>
                    <?php $i++;?>
                <?php } }?>
            </div>
            <div class="pagination">
            <?php $ta2 = $this->Paginator->param('current') + ($this->Paginator->param('page')-1) * LIMIT_PRODUCT_CATEGORY;?>
            <?php $ta1 = 1 + ($this->Paginator->param('page')-1) * LIMIT_PRODUCT_CATEGORY;?>
            <span> <?php echo 'Showing '.$ta1.' - '.$ta2.' in '.$this->Paginator->param('count').' products';?>
            <span class="pr_paginator" style="float: right;margin-bottom: 30px;margin-right:130px">
                <?php echo $this->Paginator->numbers().'  ';?>
            </span>
            </div>
        </article>
    </div>
</section>


<script type="text/javascript">
    
    function quickviewP(thiss){
        $("#quickview").remove();
        var class_show_product_view = $( thiss ).attr( "product-view" );
        var id_product = $( thiss ).attr( "id-product" );
        $.ajax({
            url: "<?php echo Router::url(array('controller' => 'products', 'action' => 'quickviewP'));  ?>",
            method: "POST",
            data: { id_products : id_product },
            dataType: "html", 
            success: function(result){
                $("."+class_show_product_view).html(result);
            }
        });        
        $(document).click(function(){
            $("#quickview").hide();
        });
    }
    
</script>
