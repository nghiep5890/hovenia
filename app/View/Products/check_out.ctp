<section id="content">
    <div class="top-banner"><!-- ///////////////////////////         BANNER         //////////////////////////      -->
        <h2>checkout</h2>
        <div class="page-link">
            <h3><?php $this->Html->link('Home', array('controller' => 'pages', 'action' => 'home')); ?>
                <i class="fa fa-caret-right fa-fw" aria-hidden="true"></i>	
                <span style="color:#fff;">checkout</span></h3>
        </div>
    </div>
    <div class="container content">
        <!-- ///////////////////////////         MENU LEFT         //////////////////////////      -->
    		<?= $this->element('aside'); ?>
        <!-- ///////////////////////////         ARTICLE         //////////////////////////      -->
        <article>
            <div class="main-checkout">
				<?php //echo $this->Form->create('product', array('class' => 'form-inline')); ?>
                <form accept-charset="utf-8" method="post" id="ProductIndexForm" action="../products/update_cart" name="Update">
                    <table>
                        <thead>
                            <tr>
                                <td class="column1">product</td>
                                <td class="column2">description</td>
                                <td class="column3">prices</td>
                                <td class="column4">quantity</td>
                                <td class="column5">totals</td>
                                <td class="column6"></td>
                            </tr>
                        </thead>
                        <tbody id="check_out">

		    			<?php if ($this->Session->check('cart')){ ?>
		    				<?php foreach ($cart as $key=>$product): ?>
                            <tr>
                                <td class="column1">
			                		<?php echo $this->Html->image('/images/products/'.$product['image']); ?>
                                </td>
                                <td class="column2">
                                    <h4><?php echo $product['name']; ?></h4>
									<?php echo $this->Html->image('/images/rate'.$product['rating'].'.png'); ?>
                                </td>
                                <td class="column3">
			                		<?php if(($product['sale_off'] > 0) && ((date('Y-m-d',strtotime($product['sale_start'])) < date('Y-m-d')) && (date('Y-m-d',strtotime($product['sale_end'])) > date('Y-m-d')))){
                                                   $price = $product['price']*(100-$product['sale_off'])/100;
                                               }else{
                                                   $price = $product['price'];
                                               }?>
                                    <input class="product-price" type="hidden" name="data[<?php echo $product['id'];?>][price]" value="<?php echo $price; ?>"/>
                                    <h3><?php echo $price; ?></h3>
                                </td>
                                <td class="column4">
                                    <div class="col col-sm-1">
                                        <i class="fa fa-caret-down down1 qd<?php echo $key?> fa-fw"></i>
                                        <input class="numeric count form-control input-small product-quantity" name="data[<?php echo $product['id'];?>][quantity]" value="<?php echo $product['quantity']; ?>" type="text"/>    
                                        <i class="fa fa-caret-up up1 qd<?php echo $key?> fa-fw"></i>
                                    </div>
                                </td>
                                <td class="column5">
                                    <h3><?php echo $this->Number->currency($price * $product['quantity'], '', array('places' => 2)); ?></h3>
                                </td>
                                <td class="column6">
			                		<?php echo $this->Form->postlink('<i class="fa fa-trash fa-1x"></i>', '/products/remove/'.$product['id'], array('escape' => false)); ?>
                                </td>
                            </tr>
							<?php endforeach; ?>

				           	<?php } else { ?>
                            <tr class="alert-cart">
                                <td colspan='6'>Empty Cart!</td>
                            </tr>
							<?php } ?>
                            <tr class="check-cart">
                                <td colspan="6">
                                    <p>
			                			<?php echo $this->Html->link('continue shopping', array('controller' => 'products', 'action' => 'index'), array('class' => 'btn-checkout btn-l', )); ?>
                                        <input type="submit" value="update your cart" class="btn-checkout btn-r" />
                                        <!-- <?php// echo $this->Form->postLink('update your cart', '/products/update_cart', array('class' => 'btn-checkout btn-r')); ?>  -->
			                			<?php echo $this->Form->postLink('Clear your cart', '/products/empty_cart', array('class' => 'clear-cart')); ?>
                                    </p>
                                </td>
                            </tr>			            
                        </tbody>
                    </table>
                </form>
				<?php //echo $this->Form->end(); ?>
            </div>
				<?php echo $this->Session->flash('addorder'); ?>
				<?php $payment = $this->Session->read('payment'); ?>
            <div class="checkform">
                <div class="checkform-">
                    <h3>discount code</h3>
					<?php echo $this->Session->Flash('coupon'); ?>
					<?php if ($this->Session->check('payment.coupon')): ?>
                    <p>Discount code was inserted!</p>
                    <small>Coupon: <?php echo $payment['coupon']; ?></small>
                    <small>Discount: <?php echo $payment['discount']; ?> %</small>
					<?php else: ?>
                    <p>Enter Discount code if you have one</p>
					<?php echo $this->Form->create('Coupon', array('controller' => 'coupons','action' => 'add', 'class' => 'form-coupon')); ?>
					<?php echo $this->Form->input('code', array('class' => 'input-coupon', 'placeholder' => '---', 'label' => false, 'div' => false)); ?>
					<?php echo $this->Form->button('apply coupon', array('type' => 'submit','class' => 'btn-checkout')); ?>
					<?php echo $this->Form->end(); ?>
				<?php endif ?>
                </div>
                <div class="checkform-">
                    <h3>personal infomation</h3>
                    <p>You’re nearly there. Just let us know some your information</p>
					<?php echo $this->Form->create('Order', array('controller' => 'orders','action' => 'checkout', 'inputDefaults' => array('label' => false))); ?>
					<?php
					$auth = AuthComponent::user();
					if (empty($auth)){
					?>
						<?php echo $this->Form->input('name' , array('placeholder' => 'Your name')); ?>
						<?php echo $this->Form->input('phone' , array('placeholder' => 'Your phone number','type'=>'number')); ?>
						<?php echo $this->Form->input('adress' , array('placeholder' => 'Your Adress')); ?>
					<?php }else{ ?>
						<?php echo $this->Form->input('name' , array('value' => $auth["last_name"].' '.$auth["first_name"])); ?>
						<?php echo $this->Form->input('phone' , array('value' => $auth["mobilephone"])); ?>
						<?php echo $this->Form->input('adress' , array('value' => $auth["address"])); ?>
					<?php } ?>
						<?php echo $this->Form->button('submit from', array('class' => 'btn-checkout')); ?>
					<?php echo $this->Form->end(); ?>
                </div>
                <div class="checkform-">
                    <h3>shopping cart total</h3>
					<?php if ($this->Session->check('payment.coupon')): ?>
                    <p>subtotal
                        <span>
						<?php echo $this->Number->currency($subtotal, '$ ', array('places' => 2)); ?>					
                        </span>
                    </p>
                    <p>discounted<span>
						<?php echo $this->Number->currency($subtotal-$payment['pay'], '$ ', array('places' => 2)); ?>
                        </span></p>
                    <p>shipping free<span>&#36;2.00</span></p>
                    <p class="checktotal">total<span><?php echo $this->Number->currency($subtotal+2-($subtotal-$payment['pay']), '$ ', array('places' => 2)); ?></span></p>
					<?php else: ?>
                    <p>subtotal<span><?php echo $this->Number->currency($subtotal, '$ ', array('places' => 2)); ?></span></p>
                    <p>shipping free<span>&#36;2.00</span></p>
                    <p class="checktotal">total<span><?php echo $this->Number->currency($subtotal + 2, '$ ', array('places' => 2)); ?></span></p>
					<?php endif ?>
					<?php echo $this->Form->create('Order', array('controller' => 'orders','action' => 'order', 'class' => 'form-add-order')); ?>
						<?php $this->Form->input('', array('type' => 'hidden')); ?>
						<?php echo $this->Form->button('proceed to checkout', array('class' => 'btn-checkout-submit')); ?>
					<?php echo $this->Form->end(); ?>
                </div>
            </div>
        </article>
    </div>
</section>
<script type="text/javascript">
    $('.down1').click(function () {
        var _this = $(this);
        var count = _this.parents('td').find('input.product-quantity').val();
        if (count > 1) {
            _this.parents('td').find('input.product-quantity').val(count - 1);

            console.log('count', count);
            var quantity = count - 1;
            var price = _this.parents('tr').find('input.product-price').val();

            var total = quantity * parseFloat(price);
            total = parseFloat(total).toFixed(2);

            _this.parents('tr').find('.column5 > h3').text(total);
        }
    });

    $('.up1').click(function () {
        var _this = $(this);
        var count = _this.parents('td').find('input.product-quantity').val();
        console.log('count', count++);
        _this.parents('td').find('input.product-quantity').val(count++);
        var quantity = count - 1;
        var price = _this.parents('tr').find('input.product-price').val();

        var total = quantity * parseFloat(price);
        total = parseFloat(total).toFixed(2);

        _this.parents('tr').find('.column5 > h3').text(total);
    });
</script>