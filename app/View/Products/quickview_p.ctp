<div id="quickview">
<form accept-charset="utf-8" method="post" id="ProductIndexForm" action="products/add_to_cart/<?php echo $productview['Product']['id'];?>">
  <input type="hidden" name="product_id" value="<?php echo $productview['Product']['id'];?>">
  <img src="../images/quickview_line.png">
  <div class="quickview">
    <div class="quickview-img">
      <div>
        <?php echo $this->Html->image('../images/products/'.$productview['Product']['path_img1'], array('width'=>'370')); ?>
      </div>
      <div class="quickview-imgs">
        <?php echo $this->Html->image('../images/products/'.$productview['Product']['path_img1']); ?>
        <?php if(!empty($productview['Product']['path_img2'])){
        echo $this->Html->image('../images/products/'.$productview['Product']['path_img2']); } ?>
        <?php if(!empty($productview['Product']['path_img3'])){
        echo $this->Html->image('../images/products/'.$productview['Product']['path_img3']);} ?>
        <?php if(!empty($productview['Product']['path_img4'])){
        echo $this->Html->image('../images/products/'.$productview['Product']['path_img4']);} ?>
      </div>
    </div>
    <div class="quickview-info">
      <div class="quickview-prod">
        <h3><?php echo $productview['Product']['name']; ?></h3>
        <p class="rate">
          <?php echo $this->Html->image('../images/rate'.$productview['Product']['rating'].'.png'); ?>3 reviews
            <span>Add your review</span>
        </p>
        <h2>
          &#36;
            <?php
             if(($productview['Product']['sale_off'] > 0) && ((date('Y-m-d',strtotime($productview['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d',strtotime($productview['Product']['sale_end'])) > date('Y-m-d')))){
                echo $productview['Product']['price']*(100-$productview['Product']['sale_off'])/100; 
            }
            else{ echo $productview['Product']['price'];
            }
            ?>
            <span>&#36;<?php echo $productview['Product']['price']; ?></span></h2>
        <p>References: <strong><?php echo $productview['Product']['references']; ?></strong></p>
        <p>Available: <font color="#52ca42"><?php echo $productview['Product']['available']; ?></font></p>
        <?php
        if(!empty($productview['Product']['expiry'])) {
        ?>
        <p class="avai">Expiry: <font color="#52ca42"><?php echo $productview['Product']['expiry']; ?> days</font></p>
        <?php } ?>
        <p><?php echo $this->Text->truncate(($productview['Product']['description']),80); ?></p>
      </div>
      <div class="quantity">
        <h3>quantity</h3>
        <p>
          <i class="fa fa-caret-down down1 fa-fw"></i>
          <input type="text" name="quantity" value="1" class="count">
          <i class="fa fa-caret-up up1 fa-fw"></i>
          <!--<input type="number" min="0" step="1" value="1" name="shoe-size">-->
        </p>
      </div>
      <div class="size">
        <h3>size</h3>
        <p>
          <!--<a href=""><i class="fa fa-caret-down fa-fw"></i></a>-->
            <select name="cars">
              <option value="s">small</option>
              <option value="m" selected="">medium</option>
              <option value="l">large</option>
            </select>
          <!--<a href=""><i class="fa fa-caret-up fa-fw"></i></a>-->
        </p>
      </div>


      <div class="quickview-cart">
        <input type="submit" value="Add To Cart" class="btn btn-add-cart"/>
        <i class="fa fa-heart fa-fw"></i>
        <p>Add to wishlist</p>
        <ul>
          <li><i class="fa fa-caret-right fa-fw"></i> 7 days returns</li>
          <li><i class="fa fa-caret-right fa-fw"></i>
                <?php  if(($productview['Product']['sale_off'] > 0) && ((date('Y-m-d',strtotime($productview['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d',strtotime($productview['Product']['sale_end'])) > date('Y-m-d')))){
                            echo $productview['Product']['sale_off']; } 
                        else { echo '0'; } ?> % off for member</li>
          <li><i class="fa fa-caret-right fa-fw"></i> Free UK Shipping</li>
          <li><i class="fa fa-caret-right fa-fw"></i><?php echo $this->Html->link(__('Shopping Guide'), array('controller' => 'pages','action' => 'shopping_guide'));  ?></a></li>
        </ul>
      </div>
      <div class="quickview-share">
        <h3>share</h3>
        <i class="fa fa-facebook fa-fw"></i>
        <i class="fa fa-twitter fa-fw"></i>
        <i class="fa fa-google-plus fa-fw"></i>
        <i class="fa fa-instagram fa-fw"></i>
        <i class="fa fa-pinterest-p fa-fw"></i>
      </div>
    </div>
  </div>
  <div class="review">
      <h3><span>description</span>review</h3>
    <p><?php echo $productview['Product']['description']; ?></p>
  </div>
  </form>
</div>
<script type="text/javascript">
  $("#quickview").click(function(e){
            e.stopPropagation();
        });
        
        $('.down1').click(function (){
            var count = $('.count').val();
            if( count > 0){
                $('.count').val(count-1);
            }
        });
        
        $('.up1').click(function (){
            var count = $('.count').val();
            console.log('count', count++);
            $('.count').val(count++);
        });
</script>