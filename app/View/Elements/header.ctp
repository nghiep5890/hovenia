<?php App::uses('AppController', 'Controller'); ?>
<header>
    <div>
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->Session->flash('login'); ?>
    </div>
    <div id="top-header">
        <div class="container">
            <div class="wellcome">
				<?php
				if (AuthComponent::user()) { ?>
                <h3>Hello 
					<?php
						$fullname = AuthComponent::user()['last_name'].' '.AuthComponent::user()['first_name'];
						echo $this->Html->link($fullname,
						array(
							'controller' => 'users',
							'action' => 'index',
							),
						array(
							'style' => 'text-transform:capitalize'
							)
						);
					?>	
                </h3>
					<?php } else { ?>
                <h3>Xin Chào, bạn có thể <a class="clicklogin" href="#">đăng ký</a> hoặc <?php echo $this->Html->link('tạo tài khoản',array('controller' => 'users','action'=>'register')); ?></h3>
					<?php } ?>
            </div>

            <div class="list-group">
                <a class="list-group-item" href="#"><i class="fa fa-heart fa-fw" aria-hidden="true"></i>&nbsp; Danh sách yêu thích</a>
					  <?php echo $this->Html->image('../images/dot.png'); ?>
                <a class="list-group-item" href="#"><i class="fa fa-retweet fa-fw" aria-hidden="true"></i>&nbsp; So Sánh</a>
					  <?php echo $this->Html->image('../images/dot.png'); ?>
                <div class="dropdown">
                    <a class="list-group-item" href="#"><i class="fa fa-user fa-fw" aria-hidden="true"></i>&nbsp; Tài Khoản</a>
		    					<?php if (AuthComponent::user()) { ?>
                    <div class="acc-down">
                        <p><?php if(AuthComponent::user()['group_id']==1){?>
                            <a href="/admin">Admin Panel</a>
                            <?php }?>
                        </p>
                        <p><?php echo $this->Html->link('Log out', array('controller' => 'users', 'action' => 'logout')); ?></p>
                    </div>
	    						<?php }else{ ?>
                    <div class="acc-down">
                        <p>Bạn phải 
							<?php echo $this->Html->link(
								'đăng nhập',
								array(
									'controller' => 'users',
									'action' => 'login'),
								array(
									'class' => 'clicklogin'
									)
							); ?> 
                            trước</p>
                    </div>
		    					<?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div id="header" class="container">
        <div id="logo" class="group-header">
            <a class="logo" href="/">
                <?php echo $this->Html->image('../images/logo-hovenia.png'); ?>
            </a>
        </div>	
        <div id="opening-hour" class="group-header">
				<?php echo $this->Html->image('../images/header_ellipse1.png', ['class'=>'ellipse1']); ?>
            <i class="fa fa-clock-o fa-fw" aria-hidden="true">&nbsp;</i>
            <div class="fa-in">
                <h3>Giờ mở cửa</h3>
                <p>Từ <?php echo $open['Setting']['value']; ?> tới <?php echo $close['Setting']['value']; ?></p>
            </div>
        </div>
        <div id="call-us" class="group-header">
				<?php echo $this->Html->image('../images/header_ellipse1.png', ['class'=>'ellipse1']); ?>
            <i class="fa fa-phone fa-fw" aria-hidden="true">&nbsp;</i>
            <div class="fa-in">
                <h3>Đường dây nóng</h3>
                <p><?php echo $phone1['Setting']['value']; ?></p>
            </div>
        </div>
        <div id="cart" class="group-header">
				<?php
				$cart = $this->Session->read('cart');
				if(empty($cart)) {
					echo $this->Html->image('../images/header_ellipse1.png',[
						'class'=>array('ellipse1','active-cart'),
						'url'=>['controller' => 'products', 'action' => 'check_out']
					]);
					?>
					<?php echo $this->Html->link('','', array('class'=>"fa fa-shopping-bag fa-fw",'controller' => 'products', 'action' => 'check_out', 'escape' => false)); ?>

					<?php
				}else{
					echo $this->Html->image('../images/header_ellipse2.png',[
						'class'=>array('ellipse1','active-cart'),
						'url'=>['controller' => 'products', 'action' => 'check_out']
					]);
					?>
					<?php echo $this->Html->link('','', array('class'=>"fa fa-shopping-bag bag-w fa-fw",'controller' => 'products', 'action' => 'check_out', 'escape' => false), array('style' => 'color:#fff;')); ?>
					<?php
				}
					?>
            <div class="fa-in">
                <h3>Giỏ hàng</h3>
					<?php $total = $this->Session->read('payment');
					?>
                <p><?php echo $total['quantity_']; ?> sản phẩm - 
					<?php
					echo $this->Number->currency($total['total']+2, '$ ', array('places' => 2));
					?>
	        		<?php echo $this->Session->flash('cart'); ?>
                </p>
            </div>
        </div>
    </div>
</header>