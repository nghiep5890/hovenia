<nav>
    <div id="nav-menu" class="container">
        <div class="icon-menu">
            <a id="c-button--push-left" class="c-button" href="#"><i class="fa fa-navicon fa-fw"></i></a>
        </div>
        <ul>
            <li><?php echo $this->Html->link('Trang Chủ', '/'); ?></li>
            <li><?php echo $this->Html->link('Về Chúng Tôi', '/about_us'); ?></li>
            <li><?php echo $this->Html->link('Sản Phẩm', '/products'); ?></li>
            <li><?php echo $this->Html->link('Chất Lượng', '/quality'); ?></li>
            <li><?php echo $this->Html->link('Hướng Dẫn Mua Hàng', '/shopping_guide'); ?></li>
            <li><?php echo $this->Html->link('Liên Hệ', array('controller' => 'pages', 'action' => 'shopping_guide#map')); ?></li>
        </ul>
        <div class="icon-search">
            <i class="fa fa-search fa-fw"></i>
        </div>
    </div>
</nav>