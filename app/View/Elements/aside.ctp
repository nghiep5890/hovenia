<aside>
    <div class="menu-left">
        <h3>menu</h3>
        <hr>
        <ul>
            <li><?php echo $this->Html->link('about us', array('controller' => 'pages', 'action' => 'about_us')); ?>
            </li>
            <li><?php echo $this->Html->link('products', array('controller' => 'products', 'action' => 'index')); ?><i class="fa fa-angle-double-down" aria-hidden="true"></i>
                <div class="menu-down">
                    <?php if($cate_aside){
                    foreach ($cate_aside as $cate_aside): ?>
                    <p><?php echo $this->Html->link($cate_aside['Category']['name'], array('controller' => 'products', 'action' => 'category','?' => array('id' => $cate_aside['Category']['id'])));?></p>
                    <?php endforeach; }?>
                </div>
            </li>
            <li><?php echo $this->Html->link('quality', array('controller' => 'pages', 'action' => 'quality')); ?>
            </li>
            <li><?php echo $this->Html->link('shopping guide', array('controller' => 'pages', 'action' => 'shopping_guide')); ?><i class="fa fa-angle-double-down" aria-hidden="true"></i>
                <div class="menu-down">
                    <p><?php echo $this->Html->link('shopping guide', array('controller' => 'pages', 'action' => 'shopping_guide#shoppingguide')); ?></p>
                    <p><?php echo $this->Html->link('payment', array('controller' => 'pages', 'action' => 'shopping_guide#payment')); ?></p>
                    <p><?php echo $this->Html->link('delivery', array('controller' => 'pages', 'action' => 'shopping_guide#delivery')); ?></p>
                    <p><?php echo $this->Html->link('good return policy', array('controller' => 'pages', 'action' => 'shopping_guide#return')); ?></p>
                    <p><?php echo $this->Html->link('Q&A', array('controller' => 'pages', 'action' => 'shopping_guide#faq')); ?></p>
                    <p><?php echo $this->Html->link('contact', array('controller' => 'pages', 'action' => 'shopping_guide#map')); ?></p>
                </div>
            </li>
            <li><?php echo $this->Html->link('shopping cart', array('controller' => 'products', 'action' => 'check_out')); ?>
            </li>
        </ul>
    </div>
    <div class="prices">
        <h3>prices</h3>
        <hr>
        <p><input type="text" id="amount" readonly style="border:0; color:#666; font-weight:bold"></p>
        <script>
            $(function () {
                var price_min = <?php echo !empty($price_min) ? $price_min : '0';?>;
                var price_max = <?php echo !empty($price_max) ? $price_max : $max_price[0][0]['max(price)'];?>;
                <?php //echo $price_max;var_dump($price_min);?>
                $("#slider-range").slider({
                    range: true,
                    min: 0,
                    max: <?php echo $max_price[0][0]['max(price)']?>,
                    values: [price_min, price_max],
                    slide: function (event, ui) { 
                        $('#price_min').val(ui.values[0]);
                        $('#price_max').val(ui.values[1]);
                        $("#amount").val("$" + ui.values[ 0 ] + ".00 - $" + ui.values[ 1 ] + ".00");
                    },
                    change: function () {
                        var min = $('#price_min').val();
                        var max = $('#price_max').val();

                        $('#ProductIndexForm').submit();
                    }
                });
                $("#amount").val("$" + $("#slider-range").slider("values", 0) + ".00 - $" + $("#slider-range").slider("values", 1) + ".00");
            });
        </script>
        <p><input type="hidden" id="amount" readonly></p>
        <form accept-charset="utf-8" method="post" id="ProductIndexForm" action="/products/search">
            <div id="slider-range"></div>
            <input type="hidden" id="price_min" name="price_min">
            <input type="hidden" id="price_max" name="price_max">
        </form>
    </div>
    <div class="recommend">
        <h3>recommend</h3>
        <?php 
            if($product_aside){
            foreach ($product_aside as $product_aside){ ?>
        <div class="aside-item-group aside-item">
            <a href="<?php echo $this->Html->url(array('controller' => 'products', 'action' => 'detail', '?' => array('id' => $product_aside['Product']['id']), )); ?>"><?php echo $this->Html->image('../images/products/'.$product_aside['Product']['path_img1'],array('height'=>'50px')); ?></a>
            <h4><a href="<?php echo $this->Html->url(array('controller' => 'products', 'action' => 'detail', '?' => array('id' => $product_aside['Product']['id']), )); ?>" style="color: black">
                <?php echo $product_aside['Product']['name']; ?>
                </a>    
            </h4>
            <h3>
                <?php if ( ($product_aside['Product']['sale_off'] <= 0) || ((date('Y-m-d',strtotime($product_aside['Product']['sale_end'])) < date('Y-m-d')) &&(date('Y-m-d',strtotime($product_aside['Product']['sale_end'])) < date('Y-m-d')))) { ?>
                        <span>&#36;<?php echo $product_aside['Product']['price'];?></span>
                    <?php } else {?> 
                        <span style="color: #CA8B5A">&#36;<?php echo $product_aside['Product']['price']*(100-$product_aside['Product']['sale_off'])/100; ?></span>
                <?php } ?>
            </h3>
        </div>
        <?php } 
        } ?>
    </div>
</aside>