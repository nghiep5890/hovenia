<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    // var $name = 'User';
    public $actsAs = array('Acl' => array('type'=>'requester'));

    // public $virtualFields = array(
    //     'fullname' => 'concat(User.last_name," ",User.fist_name)' 
    // );

    public function match_password($check, $password_field = 'password'){
        $password = $this->data['User'][$password_field];
        $confirm_password = $this->data['User']['confirm_password'];
        if (strcmp($password, $confirm_password)==0) {
            return true;
        }else{
            return false;
        }
    }
    
    public function beforeSave($options = array()) {
        // if ID is not set, we're inserting a new user as opposed to updating
        if (!$this->id) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash($this->data[$this->alias]['password']);
        }
        return true;
    }

    // public function beforeValidate($options = array()){
    //     if (isset($this->data['User']['email'])) {
    //         $user_info = AuthComponent::user();
    //         $user = $this->findById($user_info['id']);
    //         if(!empty($user_info) && $this->data['User']['email'] == $user['User']['email']){
    //             unset($this->data['User']['email']);
    //         }
    //     }
    //     return true;
    // }

    public function parentNode() {
        if (!$this->id && empty($this->data)){
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }
        if (!$data['User']['group_id']) {
            return null;
        }else{
            return array('Group' => array('id' => $data['User']['group_id']));
        }
    }

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A email is required'
            )
        )
    );

/**
 * belongsTo associations
 *
 * @var array
 */
    public $hasMany = array(
        'Review' => array(
            'className' => 'Review',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );


/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}