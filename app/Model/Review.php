<?php
App::uses('AppModel', 'Model');
class Review extends AppModel {
  
    public $belongsTo = array(

        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
        ),

        'Product' => array(
            'className' => 'Product',
            'foreignKey' => 'id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

