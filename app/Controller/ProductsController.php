<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ProductsController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *  or MissingViewException in debug mode.
     */
    public $components = array('Paginator');
    
    public function index() {
        $this->Paginator->settings = array(
            'limit' => LIMIT_PRODUCT_CATEGORY,
            'conditions' => array( 'Product.published' => 1)
        );

        $product = $this->Paginator->paginate('Product');

        $this->set('product', $product);
    }

    public function delete($id = null) {
        $this->Product->id = $id;
        if (!$this->Product->exists()) {
            throw new NotFoundException(__('Invalid Product'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Product->delete()) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function detail() {  
        $product_id = $this->request->query['id'];    
        $data = $this->Product->findById($product_id);
        $this->set('productview', $data);
    }

    public function category($id = null) {
        $category_id = isset($this->request->query['id']) ? $this->request->query['id'] : '';
        if($category_id){
            $this->Paginator->settings = array(
                'conditions' => array(
                    'OR' => array('Category.id like' => '%' . $category_id . '%')
                ),
                'limit' => LIMIT_PRODUCT_CATEGORY
             );
            
            $product_se = $this->Paginator->paginate('Product');  
            $this->set('products', $product_se);  

            $this->loadModel('Category');
            $cate = $this->Category->find("first", array('conditions' => array('id' => $category_id)));
            $this->set('category', $cate);
        }else{
            $this->set('products', null);
            $this->set('category', null);
        }
    }
    public function check_out() {
        $cart = $this->Session->read('cart');
        $subtotal = $this->Session->read('payment.total');
        $this->set(compact('cart', 'subtotal'));
        $this->set('title_for_layout', ' Store - Check Out');
    }
    
    public function quickviewP() {
        $this->layout = 'ajax';
        $idProduct = $this->request->data['id_products'];
        $productview = $this->Product->findById($idProduct);
        $this->set('productview',$productview);
    }

    public function search_product($categoryview){
        $product_se = $this->Product->find('all', array(
                                    'conditions' => array(
                                    'OR' => array('Category.name like' => '%' . $categoryview . '%'))
        ));
        
            $se = $this->request->query['Category_id'];
            $this->loadModel('Category');
            $cate = $this->Category->find("first", array('conditions' => array('id' => $se)));
            $this->set('cate', $cate);
        $this->set('product_se', $product_se);
    }


    public function add_to_cart($id = null){            
        if ($this->request->is('post')){
            // pr($this->request->is('post')); die;
            $products = $this->Product->find('first', array(
                'recursive' => -1,
                'conditions' => array('Product.id' => $id)
            ));

            
             if(($products['Product']['sale_off'] > 0) && ((date('Y-m-d',strtotime($products['Product']['sale_start'])) < date('Y-m-d')) && (date('Y-m-d',strtotime($products['Product']['sale_end'])) > date('Y-m-d')))){
                        $prices = $products['Product']['price']*(100-$products['Product']['sale_off'])/100;
                    } else { 
                        $prices = $products['Product']['price'];
                    }
            $quantity = $this->request->data['quantity']; 

            $item = array(
                'id' => $products['Product']['id'],
                'name' => $products['Product']['name'],
                'image' => $products['Product']['path_img1'],
                'rating' => $products['Product']['rating'],
                'price' => $products['Product']['price'],
                'sale_off' => $products['Product']['sale_off'],
                'sale_start' => $products['Product']['sale_start'],
                'sale_end' => $products['Product']['sale_end'],
                'prices' => $prices,
                'quantity' => $quantity
                    );
 
            $this->Session->write('cart.'.$id, $item);

            $cart = $this->Session->read('cart');
            
            $quantity_ = $this->sum_quantity($cart);
            $this->Session->write('payment.quantity_', $quantity_);

            $subtotal = $this->sum($cart);          
            $this->Session->write('payment.total', $subtotal);
            $total = $this->Session->read('payment.total');
            if ($this->Session->check('payment.coupon')) {
                $pay = $total - $total*$this->Session->read('payment.discount')/100;
                $this->Session->write('payment.pay', $pay);
            }

            $this->Session->setFlash('Add to cart successfully!', 'default', array('alert alert-info'),'cart');

            $this->redirect($this->referer());
        }
    }

    public function update_cart() {       
        if($this->request->is('post')){
             $cart = $this->Session->read('cart');
            $quantity = $this->request->data;
            if(empty($cart)){
                $this->redirect($this->referer());
            }
        if($quantity){
            foreach ($quantity as $key => $value) { 
                
                    $rows = $this->Session->read('cart.'.$key);  
                    $item = array(
                        'id' => $rows['id'],
                        'name' => $rows['name'],
                        'image' => $rows['image'],
                        'rating' => $rows['rating'],
                        'price' => $rows['price'],
                        'sale_off' => $rows['sale_off'],
                        'sale_start' => $rows['sale_start'],
                        'sale_end' => $rows['sale_end'],
                        'prices' => $value['price'],
                        'quantity' => $value['quantity']
                    );
                    $this->Session->write('cart.'.$key, $item);
                    $cart = $this->Session->read('cart');
                    $subtotal = $this->sum($cart);
                    $this->Session->write('payment.total', $subtotal);
                   
                }
            }
            $quantity_ = $this->sum_quantity($cart);
            $this->Session->write('payment.quantity_', $quantity_);
            $this->check_out();
            $this->redirect($this->referer());
        }
    }

    public function remove($id = null) {
        if($this->request->is('post')){
            $this->Session->delete('cart.'.$id);
            $cart = $this->Session->read('cart');
            if(empty($cart)){
                $this->empty_cart();
            }else{
                $subtotal = $this->sum($cart);
                $this->Session->write('payment.total', $subtotal);
                $quantity_ = $this->sum_quantity($cart);
        $this->Session->write('payment.quantity_', $quantity_);
            }
            $this->redirect($this->referer());
        }
        
        
        $this->redirect($this->referer());
    }

    public function empty_cart() {
        if($this->request->is('post')){
            $this->Session->delete('cart');
            $this->Session->delete('payment');
            return $this->redirect($this->referer());
        }
    }

    public function search() {
        $price_min = isset($this->request->data['price_min']) ? $this->request->data['price_min'] : '';
        $price_max = isset($this->request->data['price_max']) ? $this->request->data['price_max'] : '';
        $product = [];
        
        $request = $this->request->params['named'];
        $page = isset($request['page']) ? $request['page'] : '';
        
        if(empty($price_max)){
            $price_min = $this->Session->read('price_min');
            $price_max = $this->Session->read('price_max');
        }
        
        if ($price_max) {
            $this->Session->write('price_min' ,$price_min);
            $this->Session->write('price_max' ,$price_max);
            
            $sql = "price between $price_min and $price_max";
            $this->Paginator->settings = array(
                'conditions' => array($sql,'Product.published' => 1),
                'limit' => LIMIT_PRODUCT_CATEGORY
             );
            $product = $this->Paginator->paginate('Product');            
        }
        
        $this->set('price_min', $price_min);
        $this->set('price_max', $price_max);
        $this->set('product', $product);
    }

}
